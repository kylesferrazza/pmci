{ pkgs ?  import <nixpkgs> {} }:
pkgs.stdenv.mkDerivation {
  name = "pmci";
  version = "1.0";

  src = ./src;

  dev = pkgs.linux.dev;
}
