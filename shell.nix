{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  name = "pmci";
  buildInputs = with pkgs; [
    linux.dev
    ccls
    bear
  ];
  dev = pkgs.linux.dev;
}
