{
  description = "pmci (Ping Makes C2 Interesting) - a demo rootkit";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.nixpkgs-master.url = "github:NixOS/nixpkgs/master";

  outputs = { self, nixpkgs, nixpkgs-master }:
  let
    system = "x86_64-linux";
    pkgsMaster = import nixpkgs-master {
      inherit system;
    };
    pkgs = import nixpkgs {
      inherit system;
      overlays = [(prev: final: {
        bear = pkgsMaster.bear;
      })];
    };
  in {
    devShell.x86_64-linux = import ./shell.nix {
      inherit pkgs;
    };
    defaultPackage.x86_64-linux = import ./default.nix {
      inherit pkgs;
    };
  };
}
